# OaBySpringboot

### 介绍
智慧建造的oa系统的后端，采用Springboot框架搭建，配合redis以及mongodb使用

### 使用说明
Spring boot的运行模块  
为 **_`OaSystemCore-9989`_**,  
默认端口 `9989`，  
启动类为 `OaApplication`，  
`OaSelfFrameWork` 为自定义框架，  
为使用 `MongoRepository` 的  
_**dao**_，_**service**_ 以及 _**controller**_ 服务，  
提供基本的 `删改查` 方法

