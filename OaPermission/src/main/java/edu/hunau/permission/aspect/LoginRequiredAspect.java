package edu.hunau.permission.aspect;

import edu.hunau.selfframework.domain.entity.exception.BizException;
import edu.hunau.common.domain.po.User;
import edu.hunau.permission.annotation.Login;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpSession;

@Aspect
@Component
public class LoginRequiredAspect {
    @Autowired
    HttpSession session;

    @Autowired
    StringRedisTemplate redisTemplate;

    @Pointcut("@annotation(login)")
    public void loginRequired(Login login) {
    }

    @Before(value = "loginRequired(login)", argNames = "login")
    public void before(Login login){
        User user = (User) session.getAttribute("user");
        if(user==null)
            throw new BizException("登录验证失败的访问！", HttpStatus.NON_AUTHORITATIVE_INFORMATION);
        String sessionUId = user.getId();
        String redisUId = redisTemplate.opsForValue().get(session.getId());
        if(redisUId == null || !redisUId.equals(sessionUId))
            throw new BizException("登录验证失败访问！", HttpStatus.NON_AUTHORITATIVE_INFORMATION);
    }


}
