package edu.hunau.selfframework.domain.entity;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class ResultMap {
    private String msg;
    private Integer code;
    private Object data;
    private Boolean result;

    public ResultMap() {
        msg = "操作成功!";
        code = 200;
        data = null;
        result = true;
    }

    public ResultMap(Object data) {
        this.data = data;
        msg = "操作成功!";
        code = 200;
        result = true;
    }

    public ResultMap(Object data, Integer code) {
        this.data = data;
        msg = "操作成功!";
        this.code = code;
        result = true;
    }

    public ResultMap(String msg, Integer code, Object data, Boolean result) {
        this.msg = msg;
        this.code = code;
        this.data = data;
        this.result = result;
    }

    public ResultMap(Object data, String msg) {
        this.msg = msg;
        this.data = data;
        this.result = true;
        this.code = 200;
    }

    public ResultMap(Object data, Integer code, String msg) {
        this.msg = msg;
        this.data = data;
        this.code = code;
        this.result = true;
    }

    public static ResultMap puts(Object data){
        return new ResultMap(data);
    }
    public static ResultMap puts(){
        return new ResultMap();
    }

    public static ResultMap puts(Object data, Integer code, String msg, Boolean result) {
        return new ResultMap(msg, code, data, result);
    }
    public static ResultMap puts(Object data, Integer code) {
        return new ResultMap(data, code);
    }

    public static ResultMap puts(Object data, String msg) {
        return new ResultMap(data, msg);
    }

    public static ResultMap puts(Object data, Integer code, String msg){
        return new ResultMap(data, code, msg);
    }
}