package edu.hunau.selfframework.domain.entity.exception;

import org.springframework.http.HttpStatus;

public class BizException extends RuntimeException{
    private static final long serialVersionUID = 1L;

    protected Object data;

    protected HttpStatus code;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public HttpStatus getCode() {
        return code;
    }

    public void setCode(HttpStatus code) {
        this.code = code;
    }

    public BizException(Object data, HttpStatus code) {
        super("业务异常发生，查看日志以查看详情");
        this.data = data;
        this.code = code;
    }

    public BizException(String message, Object data, HttpStatus code) {
        super(message);
        this.data = data;
        this.code = code;
    }

    public BizException(String message, Throwable cause, Object data, HttpStatus code) {
        super(message, cause);
        this.data = data;
        this.code = code;
    }

    public BizException(Throwable cause, Object data, HttpStatus code) {
        super(cause);
        this.data = data;
        this.code = code;
    }

    public BizException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, Object data, HttpStatus code) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.data = data;
        this.code = code;
    }

    public BizException() {
    }

    public BizException(String message) {
        super(message);
        this.data = null;
        this.code = HttpStatus.NON_AUTHORITATIVE_INFORMATION;
    }

    public BizException(String message, Throwable cause) {
        super(message, cause);
    }

    public BizException(Throwable cause) {
        super(cause);
    }

    public BizException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    @Override
    public Throwable fillInStackTrace() {
        return this;
    }
}
