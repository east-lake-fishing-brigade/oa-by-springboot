package edu.hunau.selfframework.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Page;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MyPage<T> {
    private Long count;
    private Long totalPage;
    private Long pageNum;
    private Long pageSize;
    private Object results;


    public MyPage(Page<T> page) {
        this.count = page.getTotalElements();
        this.totalPage = (long) page.getTotalPages();
        this.pageNum = (long) page.getTotalPages();
        this.pageSize = (long) page.getSize();
        this.results = page.toList();
    }

    public MyPage(Page<T> page, List list){
        this.count = page.getTotalElements();
        this.totalPage = (long) page.getTotalPages();
        this.pageNum = (long) page.getTotalPages();
        this.pageSize = (long) page.getSize();
        this.results = list;
    }
}
