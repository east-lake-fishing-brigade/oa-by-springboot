package edu.hunau.selfframework.service;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface UpdateService<D, R extends MongoRepository<D, String>>{

    /**
     * 修改内容
     * @param entity 实体对象
     * @return 修改后的实体对象
     */
    D update(D entity);


}
