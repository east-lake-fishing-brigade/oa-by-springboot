package edu.hunau.selfframework.service.impl;

import edu.hunau.selfframework.service.RetrieveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class RetrieveServiceImpl<D, R extends MongoRepository<D, String>> implements RetrieveService<D, R> {

    @Autowired
    protected R repository;

    @Override
    public D retrieve(String  id) {
        Optional<D> byId = repository.findById(id);
        return byId.orElse(null);
    }

}
