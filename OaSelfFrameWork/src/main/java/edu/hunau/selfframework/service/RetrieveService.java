package edu.hunau.selfframework.service;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface RetrieveService<D, R extends MongoRepository<D, String>> {

    /**
     * 通过id查找
     * @param id id
     * @return 对象
     */
    D retrieve(String id);

}
