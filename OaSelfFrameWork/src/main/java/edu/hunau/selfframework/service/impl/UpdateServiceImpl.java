package edu.hunau.selfframework.service.impl;

import edu.hunau.selfframework.service.UpdateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Service;

@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class UpdateServiceImpl<D, R extends MongoRepository<D, String>> implements UpdateService<D, R> {

    @Autowired
    private R repository;

    @Override
    public D update(D entity) {
        return repository.save(entity);
    }
}
