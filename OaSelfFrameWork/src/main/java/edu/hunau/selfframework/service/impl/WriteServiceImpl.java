package edu.hunau.selfframework.service.impl;

import edu.hunau.selfframework.service.WriteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.MongoRepository;

@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class WriteServiceImpl<D, R extends MongoRepository<D, String>> implements WriteService<D, R> {

    @Autowired
    protected R repository;

    @Override
    public D update(D entity) {
        return repository.save(entity);
    }

    @Override
    public void delete(String id) {
        repository.deleteById(id);
    }
}
