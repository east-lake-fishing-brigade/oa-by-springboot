package edu.hunau.selfframework.service.impl;

import edu.hunau.selfframework.domain.entity.MyPage;
import edu.hunau.selfframework.service.ReadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class ReadServiceImpl<D, R extends MongoRepository<D, String>> implements ReadService<D, R> {


    @Autowired
    private R repository;

    @Override
    public MyPage<D> list(Integer pageSize, Integer pageNum, String order) {
        Sort sort;
        if(order.startsWith("-")){
            sort = Sort.by(Sort.Direction.DESC, order.substring(1));
        }else{
            sort = Sort.by(Sort.Direction.ASC, order);
        }
        Pageable pageable = PageRequest.of(pageNum-1, pageSize, sort);
        Page<D> all = repository.findAll(pageable);
        return new MyPage<>(all);
    }

    @Override
    public D retrieve(String id) {
        Optional<D> byId = repository.findById(id);
        return byId.orElse(null);
    }

}
