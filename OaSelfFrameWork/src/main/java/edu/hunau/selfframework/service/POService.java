package edu.hunau.selfframework.service;

import edu.hunau.selfframework.domain.entity.MyPage;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface POService<D, R extends MongoRepository<D, String>>{


    /**
     * 分页查询
     * @param pageSize 页数
     * @param pageNum 页码
     * @param order 排序字段
     * @return 页对象
     */
    MyPage<D> list(Integer pageSize, Integer pageNum, String order);

    /**
     * 通过id查找
     * @param id id
     * @return 对象
     */
    D retrieve(String id);

    /**
     * 修改内容
     * @param entity 实体对象
     * @return 修改后的实体对象
     */
    D update(D entity);

    /**
     * 删除
     * @param id id
     */
    void delete(String id);


}
