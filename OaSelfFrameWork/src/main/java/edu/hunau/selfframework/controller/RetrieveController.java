package edu.hunau.selfframework.controller;

import edu.hunau.selfframework.domain.entity.ResultMap;
import edu.hunau.selfframework.service.RetrieveService;
import edu.hunau.selfframework.utils.ResultUtil;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class RetrieveController<P, R extends MongoRepository<P, String>, S extends RetrieveService<P, R>> {
    @Autowired
    protected S service;

    @GetMapping("/{id}/")
    @ApiOperation(value = "retrieve 方法", notes = "根据 id 访问 目的对象 的详情信息")
    public ResponseEntity<ResultMap> retrieve(@PathVariable String id){
        return ResultUtil.put(service.retrieve(id), HttpStatus.OK) ;
    }
}
