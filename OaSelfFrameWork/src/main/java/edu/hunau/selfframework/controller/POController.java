package edu.hunau.selfframework.controller;

import edu.hunau.selfframework.domain.entity.ResultMap;
import edu.hunau.selfframework.service.POService;
import edu.hunau.selfframework.utils.ResultUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
@Api
public class POController<P, R extends MongoRepository<P, String>, S extends POService<P, R>> {

    @Autowired
    protected S service;

    @GetMapping(value = "/")
    @ApiOperation(value = "list方法", notes = "获得 目的对象 的list列表")
    public ResponseEntity<ResultMap> list(
            @RequestParam(value="pageSize", required=false, defaultValue="10") @ApiParam("页数大小") Integer pageSize,
            @RequestParam(value="pageNum", required=false, defaultValue="1") @ApiParam("页数") Integer pageNum,
            @RequestParam(value= "order", required=false, defaultValue="id") @ApiParam("排序字段") String order
    ){
        return ResultUtil.put(service.list(pageSize, pageNum, order), HttpStatus.MULTI_STATUS);
    }

    @GetMapping("/{id}/")
    @ApiOperation(value = "retrieve 方法", notes = "根据 id 访问 目的对象 的详情信息")
    public ResponseEntity<ResultMap> retrieve(@PathVariable String id){
        return ResultUtil.put(service.retrieve(id), HttpStatus.OK) ;
    }


    @PutMapping("/")
    @ApiOperation(value = "update方法", notes = "更改 目的对象 的信息")
    public ResponseEntity<ResultMap> update(@RequestBody P pojo){
        return ResultUtil.put(service.update(pojo), HttpStatus.ACCEPTED);
    }

    @DeleteMapping("/{id}/")
    @ApiOperation(value = "delete方法", notes = "通过id删除目的对象")
    public ResponseEntity<ResultMap> update(@PathVariable String id){
        service.delete(id);
        return ResultUtil.put(null, HttpStatus.OK);
    }


}
