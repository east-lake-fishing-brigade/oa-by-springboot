package edu.hunau.selfframework.controller;

import edu.hunau.selfframework.domain.entity.ResultMap;
import edu.hunau.selfframework.service.WriteService;
import edu.hunau.selfframework.utils.ResultUtil;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class WriteController<P, R extends MongoRepository<P, String>, S extends WriteService<P, R>> {

    @Autowired
    protected S service;

    @PutMapping("/")
    @ApiOperation(value = "update方法", notes = "更改 目的对象 的信息")
    public ResponseEntity<ResultMap> update(@RequestBody P pojo){
        return ResultUtil.put(service.update(pojo), HttpStatus.ACCEPTED);
    }

    @DeleteMapping("/{id}/")
    @ApiOperation(value = "delete方法", notes = "通过id删除目的对象")
    public ResponseEntity<ResultMap> update(@PathVariable String id){
        service.delete(id);
        return ResultUtil.put(null, HttpStatus.OK);
    }
    
}
