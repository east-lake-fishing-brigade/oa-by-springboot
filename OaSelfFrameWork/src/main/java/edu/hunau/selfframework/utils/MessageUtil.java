package edu.hunau.selfframework.utils;


public class MessageUtil {
    public final static String LOGIN_SUCCESS="登录成功！";
    public final static String LOGOUT_SUCCESS="注销成功!";
    public final static String LOGIN_ERROR="登录失败!";
    public final static String REGISTER_SUCCESS="注册成功!";
    public final static String SMS_CODE_ERROR="验证码错误或者失效，请重新发送！";
    public final static String SMS_CODE_SUCCESS="发送成功！请在手机上查收验证码!";
    public final static String GROUP_ADD_ERROR="已加入该项目！";
    public final static String GROUP_ADD_SUCCESS="加入项目成功！";
    public final static String PROJECT_CREATE_SUCCESS="创建项目成功！";
}
