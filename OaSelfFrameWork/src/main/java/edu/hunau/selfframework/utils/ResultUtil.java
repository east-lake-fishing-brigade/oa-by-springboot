package edu.hunau.selfframework.utils;


import edu.hunau.selfframework.domain.entity.ResultMap;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class ResultUtil {
    public static ResponseEntity<ResultMap> put(){
        return new ResponseEntity<>(ResultMap.puts(), HttpStatus.OK);
    }

    public static ResponseEntity<ResultMap> put(Object data){
        return new ResponseEntity<>(ResultMap.puts(data), HttpStatus.OK);
    }

    public static ResponseEntity<ResultMap> put(Object data, HttpStatus code){
        return new ResponseEntity<>(ResultMap.puts(data, code.value()), code);
    }

    public static ResponseEntity<ResultMap> put(Object data, HttpStatus code, String msg, Boolean result){
        return new ResponseEntity<>(ResultMap.puts(data, code.value(), msg, result), code);
    }

    public static ResponseEntity<ResultMap> put(Object data, String msg){
        return new ResponseEntity<>(ResultMap.puts(data, msg), HttpStatus.OK);
    }

    public static ResponseEntity<ResultMap> put(Object data, HttpStatus code, String msg){
        return new ResponseEntity<>(ResultMap.puts(data, code.value(), msg), code);
    }

}
