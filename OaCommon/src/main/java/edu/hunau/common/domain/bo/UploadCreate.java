package edu.hunau.common.domain.bo;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class UploadCreate {
    private String imgCode;
}
