package edu.hunau.common.domain.bo.user;

import lombok.Data;

@Data
public class LoginUser {
    private String tel;
    private String pwd;
}
