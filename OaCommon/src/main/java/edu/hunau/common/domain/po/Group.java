package edu.hunau.common.domain.po;

import com.fasterxml.jackson.annotation.JsonProperty;
import edu.hunau.common.domain.bo.team.group.GroupCreator;
import lombok.Data;
import lombok.NoArgsConstructor;
import nonapi.io.github.classgraph.json.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Document("group")
@Data
@NoArgsConstructor
public class Group implements Serializable {

    @Id
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private String id;

    private String uId;
    private String pId;


    public Group(GroupCreator groupCreator){
        this.pId=groupCreator.getPId();
    }

    public Group(String uId, String pId){
        this.uId=uId;
        this.pId=pId;
    }

}
