package edu.hunau.common.domain.bo.user;

public enum Role {
    BOSS("业主", "boss", 0),
    WATCHER("监理方", "watcher", 0),
    DESIGNER("设计方", "designer", 0),
    BUILDER("施工方", "builder", 0)

    ;

    Role(String name, String type, Integer level) {
        this.name = name;
        this.type = type;
        this.level = level;
    }


    private String name;
    private String type;
    private Integer level;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }
}
