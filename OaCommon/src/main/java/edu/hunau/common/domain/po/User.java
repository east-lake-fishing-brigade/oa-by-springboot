package edu.hunau.common.domain.po;

import com.fasterxml.jackson.annotation.JsonProperty;
import edu.hunau.common.domain.bo.user.LoginUser;
import edu.hunau.common.domain.bo.user.RegisterUser;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.Pattern;
import java.io.Serializable;

@Document("user")
@NoArgsConstructor
@Data
public class User implements Serializable {

    @Id
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private String id;

    private String name;

    @Pattern(regexp = "^(13[0-9]|14[01456879]|15[0-35-9]|16[2567]|17[0-8]|18[0-9]|19[0-35-9])\\d{8}$", message = "手机号格式错误！")
    @Indexed(unique = true)
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private String tel;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String pwd;

    private String role;
    private String signature;
    private String email;
    private String unitName;
    private String title;
    private String unitAddress;
    private String qualification;
    private String experience;
    private String belong;
    private String seal;


    public User(LoginUser user) {
        this.tel = user.getTel();
        this.pwd = user.getPwd();
    }

    public User(RegisterUser user) {
        this.name = user.getName();
        this.tel = user.getTel();
        this.pwd = user.getPwd();
        this.role = user.getRole();
        this.signature = user.getSignature();
        this.email = user.getEmail();
        this.unitName = user.getUnitName();
        this.title = user.getTitle();
        this.unitAddress = user.getUnitAddress();
        this.qualification = user.getQualification();
        this.experience = user.getExperience();
        this.belong = user.getBelong();
        this.seal = user.getSeal();
    }
}
