package edu.hunau.common.domain.po;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import edu.hunau.common.domain.bo.team.project.ProjectCreator;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import lombok.Data;
import lombok.NoArgsConstructor;
import nonapi.io.github.classgraph.json.Id;


@Document("project")
@Data
@NoArgsConstructor
public class Project implements Serializable{

    @Id
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private String id;
    private String name;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime = LocalDateTime.now();

    private String createUserId;
    private String description = "";
    private ArrayList<String> time;
    private JSONObject process;
    @DBRef(db = "user")
    private User createUser;


    public Project(ProjectCreator projectCreator) {
        this.name=projectCreator.getName();
        this.description=projectCreator.getDescription();
        this.time=projectCreator.getTime();
    }


}
