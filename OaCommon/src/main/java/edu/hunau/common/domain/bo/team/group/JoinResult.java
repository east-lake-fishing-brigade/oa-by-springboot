package edu.hunau.common.domain.bo.team.group;

import edu.hunau.common.domain.po.Project;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class JoinResult {
}
