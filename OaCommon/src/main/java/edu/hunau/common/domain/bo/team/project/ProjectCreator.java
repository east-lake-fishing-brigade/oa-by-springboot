package edu.hunau.common.domain.bo.team.project;

import edu.hunau.common.domain.po.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.ArrayList;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProjectCreator {
    private String name;
    private String description = "";
    private ArrayList<String> time;
}
