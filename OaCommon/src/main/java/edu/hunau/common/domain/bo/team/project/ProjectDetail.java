package edu.hunau.common.domain.bo.team.project;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.annotation.JsonFormat;
import edu.hunau.common.domain.po.Project;
import edu.hunau.common.domain.po.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.ArrayList;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProjectDetail {

    private String id;
    private String name;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;
    private User createUser;
    private String description;
    private ArrayList<String> time;
    private JSONObject process;

    public ProjectDetail(Project project){
        this.id=project.getId();
        this.name=project.getName();
        this.createTime=project.getCreateTime();
        this.description=project.getDescription();
        this.time=project.getTime();
        this.process=project.getProcess();
    }


}
