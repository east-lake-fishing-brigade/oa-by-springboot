package edu.hunau.common.domain.bo.user;

import lombok.Data;

@Data
public class RegisterUser {
    private String name;
    private String tel;
    private String pwd;
    private String role;
    private String signature;
    private String email;
    private String unitName;
    private String title;
    private String unitAddress;
    private String qualification;
    private String experience;
    private String belong;
    private String seal;
}
