package edu.hunau.common.domain.po;

import com.fasterxml.jackson.annotation.JsonProperty;
import edu.hunau.common.domain.bo.route.RouterCreate;
import edu.hunau.common.domain.bo.route.RouterMeta;
import lombok.Data;
import lombok.NoArgsConstructor;
import nonapi.io.github.classgraph.json.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.List;

@Document("router")
@Data
@NoArgsConstructor
public class Router implements Serializable {
    @Id
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private String id;
    private String path;
    private String component;
    private RouterMeta meta;
    private String role;

    @DBRef
    private List<Router> children;

    public Router(RouterCreate routerCreate){
        this.path=routerCreate.getPath();
        this.component=routerCreate.getComponent();
        this.meta=routerCreate.getMeta();
    }

}
