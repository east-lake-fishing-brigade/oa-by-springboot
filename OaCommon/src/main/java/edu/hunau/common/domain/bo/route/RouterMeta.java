package edu.hunau.common.domain.bo.route;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class RouterMeta {
    private String title;
}
