package edu.hunau.common.domain.bo.route;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class RouterCreate {
    private String path;
    private String component;
    private RouterMeta meta;
    private List<String> role;
}
