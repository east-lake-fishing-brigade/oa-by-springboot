package edu.hunau.common.domain.po;

import com.fasterxml.jackson.annotation.JsonProperty;
import edu.hunau.common.domain.bo.UploadCreate;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Document("upload")
@NoArgsConstructor
@Data
@AllArgsConstructor
public class Upload implements Serializable {
    @Id
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private String id;
    private String imgCode;
    private String uId;

    public Upload(UploadCreate uploadCreate){
        this.imgCode = uploadCreate.getImgCode();
    }


}
