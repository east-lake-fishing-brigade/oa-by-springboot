package edu.hunau.common.service.impl;

import edu.hunau.common.dao.mongo.repository.UserRepository;
import edu.hunau.selfframework.domain.entity.exception.BizException;
import edu.hunau.common.domain.bo.user.LoginUser;
import edu.hunau.common.domain.bo.user.RegisterUser;
import edu.hunau.common.domain.po.User;
import edu.hunau.common.service.UserService;
import edu.hunau.selfframework.domain.entity.MyPage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private HttpSession session;

    @Override
    public User login(LoginUser user) {
        Optional<User> one = userRepository.findOne(Example.of(new User(user)));
        try {
            session.setAttribute("user", one.get());
        }catch (Exception e){
            throw new BizException("用户名或密码错误!", HttpStatus.NON_AUTHORITATIVE_INFORMATION);
        }
        return one.orElse(null);
    }


    @Override
    public User register(RegisterUser user) {
        return userRepository.insert(new User(user));
    }

    @Override
    public MyPage<User> list(Integer pageSize, Integer pageNum, String order) {
        Sort sort;
        if(order.startsWith("-")){
            sort = Sort.by(Sort.Direction.DESC, order.substring(1));
        }else{
            sort = Sort.by(Sort.Direction.ASC, order);
        }
        Pageable pageable = PageRequest.of(pageNum-1, pageSize, sort);
        Page<User> all = userRepository.findAll(pageable);
        return new MyPage<>(all);
    }

    @Override
    public User update(User user) {
        return userRepository.save(user);
    }

    @Override
    public User retrieve(String id) {
        return userRepository.findById(id).orElse(null);
    }

    @Override
    public void delete(String id) {
        userRepository.deleteById(id);
    }

    @Override
    public Long telCount(String tel) {
        User user = new User();
        user.setTel(tel);
        return (long) userRepository.findAll(Example.of(user)).size();
    }


}
