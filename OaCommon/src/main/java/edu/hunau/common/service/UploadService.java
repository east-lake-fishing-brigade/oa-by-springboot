package edu.hunau.common.service;

import edu.hunau.common.dao.mongo.repository.UploadRepository;
import edu.hunau.common.domain.bo.UploadCreate;
import edu.hunau.common.domain.po.Upload;
import edu.hunau.selfframework.service.RetrieveService;

public interface UploadService extends RetrieveService<Upload, UploadRepository> {

    /**
     * 创建签名
     * @param uploadCreate entity对象
     * @return upload类型
     */
    Upload create(Upload uploadCreate);

}
