package edu.hunau.common.service.impl;

import edu.hunau.common.dao.mongo.repository.GroupRepository;
import edu.hunau.common.domain.po.Group;
import edu.hunau.common.service.GroupService;
import edu.hunau.selfframework.service.impl.POServiceImpl;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class GroupServiceImpl extends POServiceImpl<Group, GroupRepository> implements GroupService {

    @Override
    public Group create(Group group) {
        return repository.insert(group);
    }

    @Override
    public Boolean isAdd(Group group) {
        Optional<Group> one = repository.findOne(Example.of(group));
        Group g = one.orElse(null);
        return g!=null;
    }
}
