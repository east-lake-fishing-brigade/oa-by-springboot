package edu.hunau.common.service;

import edu.hunau.common.dao.mongo.repository.ProjectRepository;
import edu.hunau.common.domain.po.Project;
import edu.hunau.common.domain.po.User;
import edu.hunau.selfframework.domain.entity.MyPage;
import edu.hunau.selfframework.service.WriteService;

import java.util.List;

public interface ProjectService extends WriteService<Project, ProjectRepository> {

    /**
     * 创建项目
     * @param  project 项目
     * @return 创建的项目
     */
    Project create(Project project);

    /**
     * 分页查询
     * @param pageSize 页数
     * @param pageNum 页码
     * @param order 排序字段
     * @return 页对象
     */
    MyPage<Project> list(Integer pageSize, Integer pageNum, String order);


    /**
     * 获得详细信息
     * @param id id
     * @return 项目
     */
    Project retrieve(String id);


    /**
     * 通过项目id获取成员列表
     * @param id 项目id
     * @return 成员列表
     */
    List<User> getMembers(String id);


    /**
     * 获得用户参加的项目列表
     * @param uId 用户id
     * @return 项目列表
     */
    List<Project> projectsOfUser();

}
