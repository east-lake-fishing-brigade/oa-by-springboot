package edu.hunau.common.service;

import edu.hunau.common.dao.mongo.repository.GroupRepository;
import edu.hunau.common.domain.po.Group;
import edu.hunau.selfframework.service.POService;

public interface GroupService extends POService<Group, GroupRepository> {

    /**
     * 加入项目
     * @param group  加入项目
     * @return 关系
     */
    Group create(Group group);


    /**
     * 查看用户是否已加入项目
     * @param group 关系
     * @return bool
     */
    Boolean isAdd(Group group);

}
