package edu.hunau.common.service.impl;

import edu.hunau.common.dao.mongo.repository.RouterRepository;
import edu.hunau.common.domain.bo.route.RouterMeta;
import edu.hunau.common.domain.po.Router;
import edu.hunau.common.service.RouterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
public class RouterServiceImpl implements RouterService {


    @Autowired
    RouterRepository repository;

    private Router theBaseRouter(){
        Router router = new Router();
        router.setPath("/");
        router.setComponent("@/components/common/Home");
        RouterMeta routerMeta = new RouterMeta();
        routerMeta.setTitle("主页");
        router.setMeta(routerMeta);
        return router;
    }

    @Override
    public Router create(Router router) {
        return repository.insert(router);
    }

    @Override
    public List<Router> retrieve(String role) {
        Router router = new Router();
        router.setRole(role);
        List<Router> all = repository.findAll(Example.of(router));
        Router theBaseRouter = theBaseRouter();
        theBaseRouter.setChildren(all);
        return all;
    }


}
