package edu.hunau.common.service;


import edu.hunau.common.domain.po.Router;

import java.util.List;

public interface RouterService{

    Router create(Router router);

    List<Router> retrieve(String role);


}
