package edu.hunau.common.service.impl;

import edu.hunau.common.dao.mongo.repository.GroupRepository;
import edu.hunau.common.dao.mongo.repository.ProjectRepository;
import edu.hunau.common.dao.mongo.repository.UserRepository;
import edu.hunau.common.domain.po.Group;
import edu.hunau.common.domain.po.Project;
import edu.hunau.common.domain.po.User;
import edu.hunau.common.service.ProjectService;
import edu.hunau.selfframework.domain.entity.MyPage;
import edu.hunau.selfframework.service.impl.WriteServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ProjectServiceImpl extends WriteServiceImpl<Project, ProjectRepository> implements ProjectService {

    @Autowired
    StringRedisTemplate redisTemplate;

    @Autowired
    HttpSession session;

    @Autowired
    UserRepository userRepository;

    @Autowired
    GroupRepository groupRepository;


    @Override
    public Project create(Project project) {
        Project insert = repository.insert(project);
        insert.setCreateUser(userRepository.findById(insert.getCreateUserId()).orElse(null));
        return insert;
    }

    @Override
    public MyPage<Project> list(Integer pageSize, Integer pageNum, String order) {
        Sort sort;
        if(order.startsWith("-")){
            sort = Sort.by(Sort.Direction.DESC, order.substring(1));
        }else{
            sort = Sort.by(Sort.Direction.ASC, order);
        }
        Pageable pageable = PageRequest.of(pageNum-1, pageSize, sort);
        Page<Project> all = repository.findAll(pageable);
        List<Project> projects = all.toList();
        for(Project p:projects){
            Optional<User> byId = userRepository.findById(p.getCreateUserId());
            p.setCreateUser(byId.orElse(null));
        }
        return new MyPage<>(all, projects);
    }

    @Override
    public Project retrieve(String id) {
        Project project = repository.findById(id).orElse(null);
        assert project != null;
        project.setCreateUser(userRepository.findById(project.getCreateUserId()).orElse(null));
        return project;
    }

    @Override
    public List<User> getMembers(String id) {
        Group group = new Group();
        group.setPId(id);
        List<Group> all = groupRepository.findAll(Example.of(group));
        List<User> users = new ArrayList<>();
        for(Group g : all){
            users.add(userRepository.findById(g.getUId()).orElse(null));
        }
        return users;
    }

    @Override
    public List<Project> projectsOfUser() {
        String uId = redisTemplate.opsForValue().get(session.getId());
        Group group = new Group();
        group.setUId(uId);
        List<Group> all = groupRepository.findAll(Example.of(group));
        List<Project> projects = new ArrayList<>();
        for(Group g : all){
            Project project = repository.findById(g.getPId()).orElse(null);
            assert project != null;
            project.setCreateUser(userRepository.findById(project.getCreateUserId()).orElse(null));
            projects.add(project);
        }
        return projects;
    }


}