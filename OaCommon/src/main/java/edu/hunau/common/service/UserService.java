package edu.hunau.common.service;

import edu.hunau.common.domain.bo.user.LoginUser;
import edu.hunau.common.domain.bo.user.RegisterUser;
import edu.hunau.common.domain.po.User;
import edu.hunau.selfframework.domain.entity.MyPage;


public interface UserService{

    /**
     * 登录
     * @param user 登录用户 手机 与 密码 组成
     * @return 用户信息
     */
    User login(LoginUser user);

    /**
     * 注册
     * @param user 注册用户
     * @return 用户信息
     */
    User register(RegisterUser user);


    /**
     * 用户分页
     * @param pageSize 页面大小
     * @param pageNum 页数
     * @param order 排序字段
     * @return 页面对象
     */
    MyPage<User> list(Integer pageSize, Integer pageNum, String order);


    /**
     * 根据用户id 修改用户信息
     * @param user 用户
     * @return 修改后的用户对象
     */
    User update(User user);


    /**
     * 通过id访问用户信息
     * @param id id
     * @return 用户信息
     */
    User retrieve(String id);

    /**
     * 通过id删除用户信息
     * @param id id
     */
    void delete(String id);


    /**
     * 查询电话是否存在
     * @param tel 电话
     * @return 数量
     */
    Long telCount(String tel);

}
