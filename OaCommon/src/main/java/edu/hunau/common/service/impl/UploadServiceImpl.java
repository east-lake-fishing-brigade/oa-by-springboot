package edu.hunau.common.service.impl;

import edu.hunau.common.dao.mongo.repository.UploadRepository;
import edu.hunau.common.domain.po.Upload;
import edu.hunau.common.service.UploadService;
import edu.hunau.selfframework.service.impl.RetrieveServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

@Service
public class UploadServiceImpl extends RetrieveServiceImpl<Upload, UploadRepository> implements UploadService {

    @Autowired
    StringRedisTemplate redisTemplate;

    @Override
    public Upload create(Upload uploadCreate) {
        uploadCreate.setUId(redisTemplate.opsForValue().get("user"));
        return repository.insert(uploadCreate);
    }
}
