package edu.hunau.common.dao.mongo.repository;

import edu.hunau.common.domain.po.Group;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GroupRepository extends MongoRepository<Group, String> {
}
