package edu.hunau.common.dao.mongo.repository;

import edu.hunau.common.domain.po.Router;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RouterRepository extends MongoRepository<Router, String> {
}
