package edu.hunau.common.dao.mongo.repository;

import edu.hunau.common.domain.po.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends MongoRepository<User, String> {
}
