package edu.hunau.config.prop;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "tencent")
public class TencentMessageProperties {
    private int AppId;
    private String AppKey;
    private int TemplateId;
    private String signName;
}
