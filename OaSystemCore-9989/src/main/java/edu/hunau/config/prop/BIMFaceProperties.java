package edu.hunau.config.prop;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "bimface")
public class BIMFaceProperties {
    private String access;
    private String url;
    private String time;
}
