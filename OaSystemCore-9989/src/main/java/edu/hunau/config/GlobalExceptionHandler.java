package edu.hunau.config;

import edu.hunau.selfframework.domain.entity.exception.BizException;
import edu.hunau.selfframework.domain.entity.ResultMap;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.UndeclaredThrowableException;
import java.util.Objects;


@ControllerAdvice
@Log4j2
public class GlobalExceptionHandler {


    /**
     * 业务异常处理
     * @param req 请求
     * @param e 异常
     * @return 响应结果
     */
    @ExceptionHandler(value = BizException.class)
    @ResponseBody
    public ResponseEntity<ResultMap> bizExceptionHandler(HttpServletRequest req, BizException e){
        log.error("业务异常记录,原因：", e);
        return new ResponseEntity<>(ResultMap.puts(e.getData(), e.getCode().value(), e.getData().toString(), false), e.getCode());
    }

    /**
     * 一般运行时异常
     * @param req 请求
     * @param e 异常
     * @return 响应结果
     */
    @ExceptionHandler(value = RuntimeException.class)
    @ResponseBody
    public ResponseEntity<ResultMap> bizExceptionHandler(HttpServletRequest req, RuntimeException e){
        log.error("运行时异常异常记录,原因：", e);
        return new ResponseEntity<>(ResultMap.puts(e.getMessage(), HttpStatus.NON_AUTHORITATIVE_INFORMATION.value(), "操作失败！", false), HttpStatus.NON_AUTHORITATIVE_INFORMATION);
    }


    /**
     * 字段校验异常
     * @param req 请求
     * @param e 异常
     * @return 响应结果
     */
    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    @ResponseBody
    public ResponseEntity<ResultMap> validExceptionHandler(HttpServletRequest req, MethodArgumentNotValidException e){
        log.error("校验异常记录,原因：", e);
        return new ResponseEntity<>(ResultMap.puts(Objects.requireNonNull(e.getBindingResult().getFieldError()).getDefaultMessage(), HttpStatus.NON_AUTHORITATIVE_INFORMATION.value(), "字段验证错误", false), HttpStatus.NON_AUTHORITATIVE_INFORMATION);
    }

    /**
     * 普通异常处理
     * @param req 请求
     * @param e 异常
     * @return 响应结果
     */
    @ExceptionHandler(value =Exception.class)
    @ResponseBody
    public ResponseEntity<ResultMap> exceptionHandler(HttpServletRequest req, Exception e) {
        log.error("异常记录,原因：", e);
        return new ResponseEntity<>(ResultMap.puts(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR.value(), "发生异常！", false), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * 权限校验中抛出的异常
     * @param req 请求
     * @param e 异常
     * @return 响应结果
     */
    @ExceptionHandler(value = UndeclaredThrowableException.class)
    @ResponseBody
    public ResponseEntity<ResultMap> undeclaredThrowableExceptionHandler(HttpServletRequest req, UndeclaredThrowableException e) {
        log.error("异常记录,原因：", e);
        return new ResponseEntity<>(ResultMap.puts(e.getUndeclaredThrowable().getMessage(), HttpStatus.NON_AUTHORITATIVE_INFORMATION.value(), "验权异常！", false), HttpStatus.NON_AUTHORITATIVE_INFORMATION);
    }

}
