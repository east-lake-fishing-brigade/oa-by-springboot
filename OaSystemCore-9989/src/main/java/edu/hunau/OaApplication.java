package edu.hunau;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication(scanBasePackages = {"edu.hunau", "edu.hunau.common"})
@ComponentScan({"edu.hunau.common", "edu.hunau"})
@EnableMongoRepositories({"edu.hunau.common"})
@ConfigurationPropertiesScan({"edu.hunau"})
public class OaApplication {
    public static void main(String[] args) {
        SpringApplication.run(OaApplication.class, args);
    }
}
