package edu.hunau.controller.team;

import edu.hunau.common.dao.mongo.repository.ProjectRepository;
import edu.hunau.selfframework.domain.entity.exception.BizException;
import edu.hunau.common.domain.bo.team.project.ProjectCreator;
import edu.hunau.common.domain.po.Group;
import edu.hunau.common.domain.po.Project;
import edu.hunau.common.domain.po.User;
import edu.hunau.common.service.GroupService;
import edu.hunau.common.service.ProjectService;
import edu.hunau.common.service.UserService;
import edu.hunau.selfframework.controller.WriteController;
import edu.hunau.selfframework.domain.entity.ResultMap;
import edu.hunau.selfframework.utils.MessageUtil;
import edu.hunau.selfframework.utils.ResultUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

@RestController
@RequestMapping("/projects")
@Api(tags = "/projects 项目接口", description = "项目接口")
public class ProjectController extends WriteController<Project, ProjectRepository, ProjectService> {

    @Autowired
    HttpSession session;

    @Autowired
    UserService userService;

    @Autowired
    GroupService groupService;


    @PostMapping("/")
    @ApiOperation(value = "项目创建", notes = "项目创建接口")
    @Transactional
    public ResponseEntity<ResultMap> create(@RequestBody ProjectCreator project){
        Project project2;
        try{
            Project project1 = new Project(project);
            User user = (User) session.getAttribute("user");
            project1.setCreateUserId(user.getId());
            project2 = service.create(project1);
            Group group = new Group(user.getId(), project2.getId());
            groupService.create(group);
        }catch (Exception e){
            throw new BizException("加入项目失败！", HttpStatus.NON_AUTHORITATIVE_INFORMATION);
        }
        return ResultUtil.put(project2, HttpStatus.OK , MessageUtil.PROJECT_CREATE_SUCCESS);
    }

    @GetMapping(value = "/")
    @ApiOperation(value = "list方法", notes = "获得 目的对象 的list列表")
    public ResponseEntity<ResultMap> list(
            @RequestParam(value="pageSize", required=false, defaultValue="10") @ApiParam("页数大小") Integer pageSize,
            @RequestParam(value="pageNum", required=false, defaultValue="1") @ApiParam("页数") Integer pageNum,
            @RequestParam(value= "order", required=false, defaultValue="id") @ApiParam("排序字段") String order
    ){
        return ResultUtil.put(service.list(pageSize, pageNum, order), HttpStatus.MULTI_STATUS);
    }

    @GetMapping("/{id}/")
    @ApiOperation(value = "retrieve 方法", notes = "根据 id 访问 目的对象 的详情信息")
    public ResponseEntity<ResultMap> retrieve(@PathVariable String id){
        return ResultUtil.put(service.retrieve(id), HttpStatus.OK) ;
    }


    @GetMapping("/{id}/members/")
    @ApiOperation(value = "成员方法", notes = "根据项目 id 获得成员详细信息")
    public ResponseEntity<ResultMap> getMembers(@PathVariable String id){
        return ResultUtil.put(service.getMembers(id), HttpStatus.OK) ;
    }


    @GetMapping("/user/")
    @ApiOperation(value = "参与项目方法", notes = "获得用户参与的项目id")
    public ResponseEntity<ResultMap> getProjectsOfUser(){
        return ResultUtil.put(service.projectsOfUser(), HttpStatus.OK);
    }




}
