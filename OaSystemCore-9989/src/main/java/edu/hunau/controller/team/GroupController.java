package edu.hunau.controller.team;


import edu.hunau.common.dao.mongo.repository.GroupRepository;
import edu.hunau.selfframework.domain.entity.exception.BizException;
import edu.hunau.common.domain.bo.team.group.GroupCreator;
import edu.hunau.common.domain.po.Group;
import edu.hunau.common.service.GroupService;
import edu.hunau.selfframework.controller.POController;
import edu.hunau.selfframework.domain.entity.ResultMap;
import edu.hunau.selfframework.utils.MessageUtil;
import edu.hunau.selfframework.utils.ResultUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

@RestController
@RequestMapping("/groups")
@Api(tags = "/groups 入项关系接口", description = "加入项目关系接口")
public class GroupController extends POController<Group, GroupRepository, GroupService> {

    @Autowired
    HttpSession session;

    @Autowired
    private StringRedisTemplate redisTemplate;


    @PostMapping("/")
    @ApiOperation(value = "加入项目", notes = "用户加入项目")
    public ResponseEntity<ResultMap> create(GroupCreator groupCreator){
        Group group = new Group(groupCreator);
        group.setUId(redisTemplate.opsForValue().get(session.getId()));
        if(service.isAdd(group))
            throw new BizException(MessageUtil.GROUP_ADD_ERROR, HttpStatus.NON_AUTHORITATIVE_INFORMATION);
        return ResultUtil.put(service.create(group), HttpStatus.OK, MessageUtil.GROUP_ADD_SUCCESS);
    }


}
