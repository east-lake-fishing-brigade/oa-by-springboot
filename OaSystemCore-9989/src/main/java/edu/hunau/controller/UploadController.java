package edu.hunau.controller;

import edu.hunau.common.dao.mongo.repository.UploadRepository;
import edu.hunau.common.domain.bo.UploadCreate;
import edu.hunau.common.domain.po.Upload;
import edu.hunau.common.service.UploadService;
import edu.hunau.selfframework.controller.RetrieveController;
import edu.hunau.selfframework.domain.entity.ResultMap;
import edu.hunau.selfframework.utils.ResultUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/upload")
@Api(tags = "/upload 签名上传接口", description = "签名上传接口")
public class UploadController extends RetrieveController<Upload, UploadRepository, UploadService> {

    @Autowired
    StringRedisTemplate redisTemplate;

    @PostMapping("/")
    @ApiOperation(value = "签名创建接口", notes = "创建签名")
    public ResponseEntity<ResultMap> create(@RequestBody UploadCreate uploadCreate){
        Upload upload = new Upload(uploadCreate);
        upload.setUId(redisTemplate.opsForValue().get("user"));
        return ResultUtil.put(service.create(upload), HttpStatus.OK);
    }

}
