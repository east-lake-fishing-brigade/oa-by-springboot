package edu.hunau.controller;

import edu.hunau.selfframework.domain.entity.exception.BizException;
import edu.hunau.common.domain.bo.user.LoginUser;
import edu.hunau.common.domain.bo.user.RegisterUser;
import edu.hunau.common.domain.po.User;
import edu.hunau.common.service.UserService;
import edu.hunau.permission.annotation.Login;
import edu.hunau.selfframework.domain.entity.MyPage;
import edu.hunau.selfframework.domain.entity.ResultMap;
import edu.hunau.selfframework.utils.MessageUtil;
import edu.hunau.selfframework.utils.ResultUtil;
import edu.hunau.template.MessageWebSocketServer;
import edu.hunau.template.TencentSmsTemplate;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/users")
@AllArgsConstructor
@Api(tags = "/users 用户接口", description = "用户接口")
public class UserController {

    private final UserService service;

    private final StringRedisTemplate redisTemplate;

    private final HttpSession session;

    private final TencentSmsTemplate tencentTemplate;

    private final MessageWebSocketServer webSocketServer;

    private static final String USER_CHECK="user";

    @PostMapping("/login/")
    @ApiOperation(value = "登录接口", notes = "登录用户")
    public ResponseEntity<ResultMap> login(@RequestBody LoginUser loginUser){
        User userById = service.login(loginUser);
        if(userById==null)
            throw new BizException(MessageUtil.LOGIN_ERROR, HttpStatus.NON_AUTHORITATIVE_INFORMATION);
        session.setAttribute(USER_CHECK, userById);
        redisTemplate.opsForValue().set(session.getId(), userById.getId());
        return ResultUtil.put(userById, HttpStatus.ACCEPTED, MessageUtil.LOGIN_SUCCESS);
    }

    @GetMapping("/test/")
    public ResponseEntity<ResultMap> test(){
        User user = new User();
        user.setName("afsdfasdfasfasfas");
        user.setPwd("1243123");
        webSocketServer.sendMessage(user, session);
        return ResultUtil.put(null, HttpStatus.ACCEPTED, MessageUtil.LOGIN_SUCCESS);
    }

    @GetMapping("/logout/")
    @ApiOperation(value = "注销接口", notes = "注销用户")
    public ResponseEntity<ResultMap> logout(){
        session.removeAttribute(USER_CHECK);
        redisTemplate.delete(session.getId());
        return ResultUtil.put(null, HttpStatus.OK, MessageUtil.LOGOUT_SUCCESS);
    }

    @PostMapping("/register/")
    @ApiOperation(value = "注册接口", notes = "注册用户")
    public ResponseEntity<ResultMap> register(@RequestBody RegisterUser loginUser){
        User user = service.register(loginUser);
        return ResultUtil.put(user, HttpStatus.ACCEPTED, MessageUtil.REGISTER_SUCCESS);
    }

    @GetMapping("/{tel}/count/")
    @ApiOperation(value = "电话查询", notes = "查询电话数量的接口")
    public ResponseEntity<ResultMap> telCount(@PathVariable String tel){
        Long aLong = service.telCount(tel);
        Map<String, Long> result = new HashMap<>();
        result.put("count", aLong);
        return ResultUtil.put(result, HttpStatus.OK);
    }


    @PostMapping("/{smsCode}/register/")
    @ApiOperation(value = "验证码注册", notes = "通过验证码进行注册")
    public ResponseEntity<ResultMap> registerWithCode(@RequestBody RegisterUser loginUser, @PathVariable String smsCode){
        if (!smsCode.equals(redisTemplate.opsForValue().get(USER_CHECK+loginUser.getTel())))
                throw new BizException(MessageUtil.SMS_CODE_ERROR, HttpStatus.NON_AUTHORITATIVE_INFORMATION);
        User user = service.register(loginUser);
        redisTemplate.delete(USER_CHECK+loginUser.getTel());
        return ResultUtil.put(user, HttpStatus.ACCEPTED, MessageUtil.REGISTER_SUCCESS);
    }


    @GetMapping("/{tel}/smsCode/")
    @ApiOperation(value = "获得验证码", notes = "通过手机号获得验证码")
    public ResponseEntity<ResultMap>  getCode(@PathVariable String tel){
        String code = Integer.toString((int) ((Math.random() * 9 + 1) * 10000));
        tencentTemplate.sendMesModel(tel, code);
        redisTemplate.opsForValue().set(USER_CHECK+tel, code);
        return ResultUtil.put(null, MessageUtil.SMS_CODE_SUCCESS);
    }

    @GetMapping("/")
    @Login
    @ApiOperation(value = "list方法", notes = "获得 用户对象 的list列表")
    public ResponseEntity<ResultMap> list(
            @RequestParam(value="pageSize", required=false, defaultValue="10") @ApiParam("页数大小") Integer pageSize,
            @RequestParam(value="pageNum", required=false, defaultValue="1") @ApiParam("页数") Integer pageNum,
            @RequestParam(value= "order", required=false, defaultValue="id") @ApiParam("排序字段") String order
    ){
        MyPage<User> list = service.list(pageSize, pageNum, order);
        return ResultUtil.put(list, HttpStatus.OK);
    }

    @GetMapping("/{id}/")
    @ApiOperation(value = "retrieve方法", notes = "获得 用户对象 的详细信息")
    public ResponseEntity<ResultMap> retrieve(@PathVariable String id){
        User retrieve = service.retrieve(id);
        return ResultUtil.put(retrieve, HttpStatus.OK);
    }

    @PutMapping("/")
    @ApiOperation(value = "update方法", notes = "更改 用户对象 的信息")
    public ResponseEntity<ResultMap> update(@RequestBody User user){
        User update = service.update(user);
        return ResultUtil.put(update, HttpStatus.ACCEPTED);
    }


    @DeleteMapping("/{id}/")
    @ApiOperation(value = "delete方法", notes = "通过id删除用户对象")
    public ResponseEntity<ResultMap> update(@PathVariable String id){
        service.delete(id);
        return ResultUtil.put(null, HttpStatus.OK);
    }

}
