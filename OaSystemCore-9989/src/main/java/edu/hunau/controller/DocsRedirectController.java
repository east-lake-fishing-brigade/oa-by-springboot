package edu.hunau.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@SuppressWarnings("ALL")
@Controller
@Api(tags = "文档重定向", description = "文档重定向")
public class DocsRedirectController {

    @GetMapping("/")
    @ApiOperation(value = "首页重定向-swagger-ui", notes = "重定向到 /doc.html")
    public String swaggerUi(){
        return "redirect:/doc.html";
    }

}
