package edu.hunau.controller;

import edu.hunau.selfframework.domain.entity.ResultMap;
import edu.hunau.selfframework.utils.ResultUtil;
import edu.hunau.template.AccessTokenTemplate;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/tokens")
@AllArgsConstructor
@Api(tags = "/tokens AccessToken 接口", description = "token 接口")
public class AccessTokenController {

    private final AccessTokenTemplate accessTokenTemplate;

    @GetMapping("/get/")
    public ResponseEntity<ResultMap> getToken(){
        Map token = accessTokenTemplate.getToken();
        return ResultUtil.put(token);
    }
}
