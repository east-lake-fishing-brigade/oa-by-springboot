package edu.hunau.controller;

import edu.hunau.common.domain.bo.route.RouterCreate;
import edu.hunau.common.domain.po.Router;
import edu.hunau.common.service.RouterService;
import edu.hunau.selfframework.domain.entity.ResultMap;
import edu.hunau.selfframework.utils.ResultUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/routes")
@Api(tags = "/routes 路由接口", description = "路由接口")
public class RouteController {

    @Autowired
    RouterService service;

    @PostMapping("/")
    @ApiOperation(value = "路由注册接口", notes = "注册路由")
    public ResponseEntity<ResultMap> create(@RequestBody RouterCreate routerCreate){
        Router router = new Router(routerCreate);
        List<Router> list = new ArrayList<>();
        for(String role: routerCreate.getRole()){
            router.setRole(role);
            list.add(service.create(router));
            router = new Router(routerCreate);
        }
        return ResultUtil.put(list, HttpStatus.OK);
    }

    @GetMapping("/{role}/")
    @ApiOperation(value = "路由获取接口", notes = "根据身份等级获取路由列表")
    public ResponseEntity<ResultMap> retrieve(@PathVariable String role){
        return ResultUtil.put(service.retrieve(role), HttpStatus.OK);
    }


}
