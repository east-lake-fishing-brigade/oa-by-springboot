package edu.hunau.template;

import edu.hunau.common.domain.po.User;
import edu.hunau.selfframework.domain.entity.exception.BizException;
import edu.hunau.selfframework.utils.RedisUtil;
import edu.hunau.selfframework.utils.ws.ObjectEncoder;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.DependsOn;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpSession;
import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

@Component
@DependsOn("redisUtil")
@ServerEndpoint(value = "/message/{userId}", encoders = {ObjectEncoder.class})
@Log4j2
public class MessageWebSocketServer {

    private static final String USER_CHEK = "user_websocket_";

    public static AtomicInteger onlineCount = new AtomicInteger();

    public static Map<String, Session> clientMap = new ConcurrentHashMap<>();

    public final StringRedisTemplate redisTemplate = RedisUtil.redis;

    @OnOpen
    public void onOpen(Session session, @PathParam("userId") String userId){
        init(userId, session);
        log.info("客户端："+USER_CHEK+session.getId()+"，由用户："+userId+"连接！");
    }

    @OnClose
    public void onClose(Session session) {
        close(session);
        log.info("客户端："+USER_CHEK+session.getId()+"，断开连接！");
    }

    @OnMessage
    public void onMessage(String message, Session session){
        System.out.println(session.getId()+"发送消息："+message);
    }

    public void sendMessage(Object message, HttpSession session) {
        try {
            Session toSession = getSession(getUserId(session));
            toSession.getBasicRemote().sendObject(message);
        } catch (IOException | EncodeException e) {
            throw new BizException("websocket消息发送失败！", e);
        }
    }

    private String  getUserId(HttpSession session){
        return redisTemplate.opsForValue().get(session.getId());
    }

    private String getUserId(Session session){
        return redisTemplate.opsForValue().get(USER_CHEK + session.getId());
    }

    private void setUserId(String userId, Session session){
        redisTemplate.opsForValue().set(USER_CHEK+session.getId(), userId);
    }

    private void removeUserId(Session session){
        redisTemplate.delete(USER_CHEK+session.getId());
    }

    private Session getSession(String userId){
        return clientMap.get(userId);
    }


    public void init(String userId, Session session){
        onlineCount.incrementAndGet();
        clientMap.put(userId, session);
        setUserId(userId, session);
    }

    public void close(Session session){
        onlineCount.decrementAndGet();
        String userId = getUserId(session);
        clientMap.remove(userId);
        removeUserId(session);
    }

}
