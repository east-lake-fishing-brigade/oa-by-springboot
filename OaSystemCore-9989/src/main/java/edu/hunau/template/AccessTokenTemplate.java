package edu.hunau.template;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import edu.hunau.config.prop.BIMFaceProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;

@Component
public class AccessTokenTemplate {

    private static final String HEAD = "Authorization";
    private static final String KEY = "AccessToken";
    private static final String TIME = "expireTime";
    private static final String DATA = "data";

    @Autowired
    private BIMFaceProperties properties;

    @Autowired
    private StringRedisTemplate redisTemplate;


    @Autowired
    private RestTemplate restTemplate;

    public Map<String,String> getToken(){
        String accessToken = redisTemplate.opsForValue().get(KEY);
        if (compareTime(accessToken))
            return JSON.parseObject(accessToken, Map.class);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(HEAD, properties.getAccess());
        ResponseEntity<String> stringResponseEntity = restTemplate.postForEntity(properties.getUrl(), new HttpEntity<String>(httpHeaders), String.class);
        Map<String, Object> json = JSON.parseObject(stringResponseEntity.getBody(), Map.class);
        Map<String, String> map = JSON.parseObject(json.get(DATA).toString(), Map.class);
        redisTemplate.opsForValue().set(
                KEY,
                JSON.parseObject(json.get(DATA).toString(), String.class),
                Duration.ofSeconds(getSeconds(LocalDateTime.now(), json))
        );
        return map;
    }


    public boolean compareTime(String token){
        if (token!=null){
            Map<String, String> map = JSON.parseObject(token, Map.class);
            String expireTime = map.get(TIME);
            DateTimeFormatter df = DateTimeFormatter.ofPattern(properties.getTime());
            LocalDateTime expire = LocalDateTime.parse(expireTime, df);
            return expire.isAfter(LocalDateTime.now());
        }
        return false;
    }

    public long getSeconds(LocalDateTime localDateTime, Map<String,Object> json){
        JSONObject data = (JSONObject) json.get(DATA);
        String expireTime = data.get(TIME).toString();
        DateTimeFormatter df = DateTimeFormatter.ofPattern(properties.getTime());
        LocalDateTime expire = LocalDateTime.parse(expireTime, df);
        Duration duration = Duration.between(localDateTime, expire);
        return duration.getSeconds();
    }


}
