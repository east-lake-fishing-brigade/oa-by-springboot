package edu.hunau.template;

import com.alibaba.fastjson.JSONException;
import com.github.qcloudsms.SmsSingleSender;
import com.github.qcloudsms.SmsSingleSenderResult;
import com.github.qcloudsms.httpclient.HTTPException;
import edu.hunau.selfframework.domain.entity.exception.BizException;
import edu.hunau.config.prop.TencentMessageProperties;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class TencentSmsTemplate {
    private final TencentMessageProperties txProperties;

    public TencentSmsTemplate(TencentMessageProperties txProperties) {
        this.txProperties = txProperties;
    }

    /**
     * 指定正文模板id发送短信
     * @param number 用户的手机号码
     */
    public void sendMesModel(String number, String value) {
        try {
            // 接收生成的验证码，设置5分钟内填写
            String[] params = {value, "5"};
            // 构建短信发送器
            SmsSingleSender ssender = new SmsSingleSender(txProperties.getAppId(), txProperties.getAppKey());
            SmsSingleSenderResult result = ssender.sendWithParam("86", number,
                    txProperties.getTemplateId(), params, txProperties.getSignName(), "", ""); // 签名参数未提供或者为空时，会使用默认签名发送短信
        } catch (HTTPException e) {
            throw new BizException("短信发送失败,HTTP响应码错误!", HttpStatus.NON_AUTHORITATIVE_INFORMATION);
        } catch (JSONException e) {
            throw new BizException("短信发送失败,json解析错误!", HttpStatus.NON_AUTHORITATIVE_INFORMATION);
        } catch (IOException e) {
            throw new BizException("短信发送失败,网络IO错误!", HttpStatus.NON_AUTHORITATIVE_INFORMATION);
        }
    }
}
